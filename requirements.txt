requests==2.26.0
numpy==1.21.1
flake8==3.9.2
coverage==5.5
bandit==1.7.0
