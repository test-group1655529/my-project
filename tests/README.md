# My Simple Application

This is a simple application that demonstrates a basic GitLab CI/CD pipeline.

## Description

The application contains code files and a pipeline for building, testing, and deploying the application.

## Usage

- Run the main program: `python main.py`
- Run the tests: `python -m unittest test_main.py`
